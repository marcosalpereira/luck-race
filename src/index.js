function init() {
    const playersEls = document.querySelectorAll(`.players .player`);
    playersEls.forEach((input, i) => {
        const name = localStorage.getItem(`luckrace.players.${i}`);
        input.value = name;
    });
}
class Car {
    constructor(id, label = id, score = 0, lap = 1) {
        this.id = id;
        this.label = label;
        this.score = score;
        this.lap = lap;
    }
}
class Race {
    constructor(cars, laps = 3, lap = 0) {
        this.cars = cars;
        this.laps = laps;
        this.lap = lap;
    }
}
const advance = () => {
    const icar = Math.floor(Math.random() * race.cars.length);
    advanceCar(icar, 1);
};
const advanceCar = (icar, step) => {
    const car = race.cars[icar];
    car.score += step;
    if (car.score % 100 === 0)
        car.lap++;
    updateCar(car);
    updateScores();
};
let race;
let paused = false;
const onPause = () => {
    paused = !paused;
};
const onStart = () => {
    paused = false;
    const cars = [];
    const playersEls = document.querySelectorAll(`.players .player`);
    playersEls.forEach((input, i) => {
        const name = input.value;
        if (name === null || name === void 0 ? void 0 : name.length) {
            cars.push(new Car(String.fromCharCode(97 + i), name));
            localStorage.setItem(`luckrace.players.${i}`, name);
        }
        else {
            localStorage.setItem(`luckrace.players.${i}`, '');
        }
    });
    const lapsEl = document.querySelector(`.game-control .laps`);
    const laps = (lapsEl === null || lapsEl === void 0 ? void 0 : lapsEl.value) || '3';
    race = new Race(cars, +laps);
    document.querySelectorAll(`.scores ul`)
        .forEach((e) => e.style.display = 'block');
    setInterval(() => {
        tick();
    }, 50);
};
const tick = () => {
    if (!paused)
        advance();
};
function updateScores() {
    const sorted = race.cars.slice()
        .sort((a, b) => b.score - a.score);
    const lis = document.querySelectorAll(`.scores li`);
    lis.forEach((li, i) => {
        if (i < sorted.length) {
            let dif = '';
            if (i > 0) {
                dif = '[' + (sorted[i].score - sorted[0].score) + ']';
            }
            li.innerHTML = `${sorted[i].label} ${dif}`;
        }
    });
    race.lap = sorted[0].lap;
    if (race.lap > race.laps) {
        paused = true;
        setContent('.scores .winner', sorted[0].label);
    }
    if (race.lap <= race.laps) {
        setContent('.scores .lap', sorted[0].lap + '/' + race.laps);
    }
}
function setContent(selector, value) {
    const el = document.querySelector(selector);
    if (el) {
        el.innerHTML = value;
    }
}
function updateCar(car) {
    const carEl = document.querySelector(`.car.${car.id}`);
    if (!carEl)
        return;
    let p = car.score % 100;
    carEl.setAttribute('p', `${p}`);
    if ((p >= 1 && p <= 12) || (p >= 88 && p <= 100)) {
        carEl.setAttribute('pos', 'top');
    }
    if ((p >= 13 && p <= 37)) {
        carEl.setAttribute('pos', 'right');
    }
    if ((p >= 38 && p <= 62)) {
        carEl.setAttribute('pos', 'bottom');
    }
    if ((p >= 63 && p <= 87)) {
        carEl.setAttribute('pos', 'left');
    }
}
